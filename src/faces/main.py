import base64
import io
import os.path
import typing as t

import aioboto3
import aiohttp

from .base import AnalyzeSpec
from .base import Context
from .base import EventMessage
from .base import Feature
from .base import Response
from .base import VisionRequest
from .images import crop_face
from .images import request_faces
from .keygen import generate_id
from .keygen import verify_key
from .settings import Settings


async def handler(event: dict[str, t.Any], context: Context):
    msg: EventMessage = EventMessage.parse_obj(event['messages'].pop())

    settings = Settings()
    boto_session = aioboto3.Session(
        aws_access_key_id=settings.aws_access_key_id,
        aws_secret_access_key=settings.aws_secret_access_key,
        region_name=settings.aws_region_name,
    )
    if verify_key(msg.details.object_id):
        return Response(
            statusCode=200,
            body=f'Already cropped {msg.details.object_id}',
        ).dict(by_alias=True, exclude_unset=True)

    async with boto_session.client(
        's3',
        endpoint_url=settings.aws_endpoint_url,
    ) as s3client:
        fileobj = io.BytesIO()

        await s3client.download_fileobj(
            msg.details.bucket_id,
            msg.details.object_id,
            fileobj,
        )
        fileobj.seek(0)
        req = VisionRequest(
            folderId=msg.event_metadata.folder_id,
            analyze_specs=[
                AnalyzeSpec(
                    content=base64.b64encode(fileobj.read()),
                    features=[Feature()],
                ),
            ],
        )
        auth_token = (
            f"{context.token['token_type']} {context.token['access_token']}"
        )
        headers = {
            'Authorization': auth_token,
        }
        async with aiohttp.ClientSession() as session:
            resp = await request_faces(
                session,
                settings.vision_endpoint_url,
                headers,
                req.dict(),
            )
        sub_files = []
        for index, face in enumerate(resp.get_faces()):
            fileobj.seek(0)
            res = crop_face(fileobj, face)
            res.seek(0)
            name, ext = os.path.splitext(msg.details.object_id)
            id = generate_id(msg.details.object_id)
            s3_key = f'{name}_{index}_{id}{ext}'
            sub_files.append(s3_key)
            await s3client.upload_fileobj(
                res,
                msg.details.bucket_id,
                s3_key,
            )

    async with boto_session.client(
        'sqs',
        endpoint_url=settings.sqs_endpoint_url,
        region_name=settings.aws_region_name,
    ) as sqs_client:
        response = await sqs_client.get_queue_url(
            QueueName=settings.sqs_queue_name,
        )
        queue_url = response['QueueUrl']
        message = ' '.join(sub_files)
        await sqs_client.send_message(
            QueueUrl=queue_url,
            MessageBody=message,
        )

    return Response(
        statusCode=200,
        body=f'Cropped for {msg.details.object_id}',
    ).dict(by_alias=True, exclude_unset=True)
