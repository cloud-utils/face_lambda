import base64
import re


def generate_id(key: str) -> str:
    return base64.b64encode(key.encode()).decode()[:5]


def verify_key(key: str) -> bool:
    return bool(re.search(generate_id(key), key))
