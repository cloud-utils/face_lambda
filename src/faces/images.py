import io
import typing as t

import aiohttp
from PIL import Image
from PIL import ImageDraw

from .base import VisionResponse


async def request_faces(
    session: aiohttp.ClientSession,
    url: str,
    headers: dict[str, str],
    json: dict[str, t.Any],
) -> VisionResponse:
    async with session.post(url, headers=headers, json=json) as resp:
        return VisionResponse(**await resp.json())


def crop_face(
    image: io.BytesIO,
    vertices: list[tuple[int, int]],
) -> t.IO[bytes]:
    fileobj = io.BytesIO()
    original = Image.open(image)
    mask = Image.new('L', original.size, 0)
    draw = ImageDraw.Draw(mask)
    draw.polygon(vertices, fill=255, outline=None)
    black = Image.new('RGB', original.size, 0)
    result = Image.composite(original, black, mask)
    result.save(fileobj, 'JPEG')
    return fileobj
