from pydantic import BaseSettings
from pydantic import HttpUrl
from pydantic.env_settings import SettingsSourceCallable


class Settings(BaseSettings):
    aws_access_key_id: str
    aws_secret_access_key: str
    aws_region_name: str
    aws_endpoint_url: HttpUrl
    sqs_endpoint_url: HttpUrl
    sqs_queue_name: str
    vision_endpoint_url: HttpUrl = (
        'https://vision.api.cloud.yandex.net/'  # type: ignore[assignment]
        'vision/v1/batchAnalyze'
    )

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'

        @classmethod
        def customise_sources(
            cls,
            init_settings: SettingsSourceCallable,
            env_settings: SettingsSourceCallable,
            file_secret_settings: SettingsSourceCallable,
        ) -> tuple[SettingsSourceCallable, ...]:
            return (
                env_settings,
                init_settings,
                file_secret_settings,
            )
