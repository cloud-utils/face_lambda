import datetime
import typing as t

from pydantic import BaseModel


def to_camel(string: str) -> str:
    words = string.split('_')
    return words[0] + ''.join(word.title() for word in words[1:])


class Details(BaseModel):
    bucket_id: str
    object_id: str


class TracingContext(BaseModel):
    trace_id: str
    spand_id: t.Optional[str]
    parent_span_id: t.Optional[str]


class EventMetadata(BaseModel):
    event_id: str
    event_type: str
    created_at: datetime.datetime
    tracing_context: TracingContext
    cloud_id: str
    folder_id: str


class EventMessage(BaseModel):
    event_metadata: EventMetadata
    details: Details


class IAMToken(t.TypedDict):
    access_token: str
    expires_in: int
    token_type: str


class Context(t.NamedTuple):
    token: IAMToken


class Feature(BaseModel):
    type: str = 'FACE_DETECTION'


class AnalyzeSpec(BaseModel):
    content: str
    features: list[Feature]


class VisionRequest(BaseModel):
    folderId: str
    analyze_specs: list[AnalyzeSpec]


class Vertex(BaseModel):
    x: int
    y: int


class BoundingBox(BaseModel):
    vertices: list[Vertex]


class Face(BaseModel):
    bounding_box: BoundingBox

    class Config:
        alias_generator = to_camel


class FaceDetection(BaseModel):
    faces: list[Face]


class VisionInnerResult(BaseModel):
    face_detection: FaceDetection

    class Config:
        alias_generator = to_camel


class VisionOutResult(BaseModel):
    results: list[VisionInnerResult]


class VisionResponse(BaseModel):
    results: list[VisionOutResult]

    class Config:
        alias_generator = to_camel

    def get_faces(self) -> list[list[tuple[int, int]]]:
        faces = []
        for f in self.results[0].results[0].face_detection.faces:
            faces.append(
                list(map(lambda e: (e.x, e.y), f.bounding_box.vertices)),
            )
        return faces


class Response(BaseModel):
    status_code: int
    headers: t.Optional[dict[str, str]]
    multi_value_headers: t.Optional[dict[str, str]]
    body: str
    is_base64_encoded: bool = False

    class Config:
        alias_generator = to_camel
